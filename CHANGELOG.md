# Changelog

Todas as mudanças notáveis ​​neste projeto serão documentadas neste arquivo.

O formato é baseado em [Keep a Changelog](https://keepachangelog.com/), e este projeto adere ao [Versionamento Semântico](https://semver.org/lang/pt-BR/).

## [1.1.0] - 2024-07-28

### Adicionado

- Adição de changelog no projeto.

## [Padrão de escrita]

### Adicionado

- Adição de novas funcionalidades.
- Descrição das novas features ou componentes.

### Alterado

- Modificações em funcionalidades existentes.
- Melhorias na performance ou usabilidade.

### Corrigido

- Correções de bugs.
- Descrição dos problemas resolvidos.

### Removido

- Funcionalidades ou componentes que foram removidos.
- Explicação das remoções.

## [1.0.0] - YYYY-MM-DD

### Adicionado

- Implementação inicial do projeto.
- Adição das primeiras funcionalidades.

### Alterado

- Descrição de qualquer mudança significativa na versão inicial.

### Corrigido

- Qualquer correção feita antes do lançamento da versão 1.0.0.

## [0.1.0] - YYYY-MM-DD

### Adicionado

- Projeto iniciado.
- Primeiras funcionalidades básicas.
