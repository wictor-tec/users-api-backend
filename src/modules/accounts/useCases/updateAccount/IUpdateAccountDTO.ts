export interface IUpdateAccountDTO {
  id: string;
  title: string;
  amount: number;
  description: string;
  icon: string;
  user_id: string;
}
