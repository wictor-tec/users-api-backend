import { AppError } from '../../../../shared/errors/AppError';

export namespace DeleteAccountError {
  export class AccountNotFound extends AppError {
    constructor() {
      super('Account not found', 404);
    }
  }

  export class UserNotAllowed extends AppError {
    constructor() {
      super('User not allowed', 401);
    }
  }
}
