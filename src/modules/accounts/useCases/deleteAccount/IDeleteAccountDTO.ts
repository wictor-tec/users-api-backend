export interface IDeleteAccountDTO {
  id: string;
  user_id: string;
}
