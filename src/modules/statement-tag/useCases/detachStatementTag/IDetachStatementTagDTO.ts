export interface IDetachStatementTagDTO {
  statement_id: string;
  tag_id: string;
}
