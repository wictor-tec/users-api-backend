export interface IDeleteStatementTagDTO {
  user_id: string;
  tag_id: string;
}
