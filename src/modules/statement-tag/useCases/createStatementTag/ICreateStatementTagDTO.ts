export interface ICreateStatementTagDTO {
  user_id: string;
  description: string;
}
