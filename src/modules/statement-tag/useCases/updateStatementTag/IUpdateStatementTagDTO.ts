export interface IUpdateStatementTagDTO {
  description: string;
  user_id: string;
  tag_id: string;
}
