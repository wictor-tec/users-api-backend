export interface IGetBalanceDTO {
  user_id: string;
  account_id: string;
  with_statement?: boolean;
}
